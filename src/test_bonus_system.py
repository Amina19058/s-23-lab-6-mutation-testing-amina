from bonus_system import calculateBonuses
import pytest

@pytest.mark.parametrize(
    "program, amount, expected", 
    [
        ("Standard", 0, 0.5),
        ("Standard", 5000, 0.5),
        ("Standard", 9999, 0.5),
        ("Standard", 10000, 0.5 * 1.5),
        ("Standard", 10001, 0.5 * 1.5),
        ("Standard", 25000, 0.5 * 1.5),
        ("Standard", 49999, 0.5 * 1.5),
        ("Standard", 50000, 0.5 * 2),
        ("Standard", 50001, 0.5 * 2),
        ("Standard", 75000, 0.5 * 2),
        ("Standard", 99999, 0.5 * 2),
        ("Standard", 100000, 0.5 * 2.5),
        ("Standard", 100001, 0.5 * 2.5),
        ("Standard", 125000, 0.5 * 2.5),

        ("Premium", 0, 0.1),
        ("Premium", 5000, 0.1),
        ("Premium", 9999, 0.1),
        ("Premium", 10000, 0.1 * 1.5),
        ("Premium", 10001, 0.1 * 1.5),
        ("Premium", 25000, 0.1 * 1.5),
        ("Premium", 49999, 0.1 * 1.5),
        ("Premium", 50000, 0.1 * 2),
        ("Premium", 50001, 0.1 * 2),
        ("Premium", 75000, 0.1 * 2),
        ("Premium", 99999, 0.1 * 2),
        ("Premium", 100000, 0.1 * 2.5),
        ("Premium", 100001, 0.1 * 2.5),
        ("Premium", 125000, 0.1 * 2.5),

        ("Diamond", 0, 0.2),
        ("Diamond", 5000, 0.2),
        ("Diamond", 9999, 0.2),
        ("Diamond", 10000, 0.2 * 1.5),
        ("Diamond", 10001, 0.2 * 1.5),
        ("Diamond", 25000, 0.2 * 1.5),
        ("Diamond", 49999, 0.2 * 1.5),
        ("Diamond", 50000, 0.2 * 2),
        ("Diamond", 50001, 0.2 * 2),
        ("Diamond", 75000, 0.2 * 2),
        ("Diamond", 99999, 0.2 * 2),
        ("Diamond", 100000, 0.2 * 2.5),
        ("Diamond", 100001, 0.2 * 2.5),
        ("Diamond", 125000, 0.2 * 2.5),

        ("", 0, 0),
        ("hello", 5000, 0),
        ("123", 9999, 0),
        ("", 10000, 0),
        ("smth", 10001, 0),
        ("}", 25000, 0),
        ("", 49999, 0),
        ("678", 50000, 0),
        (".j", 50001, 0),
        ("", 75000, 0),
        ("Standar", 99999, 0),
        ("Premiu", 100000, 0),
        ("", 100001, 0),
        ("Diamon", 125000, 0),   
    ]
)


def test_bonus_system(program, amount, expected):
    result = calculateBonuses(program, amount)
    assert result == expected
